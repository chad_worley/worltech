$sc_gateway_username = "NiftyPay ID here";
$sc_order_script_url = "https://secure.paymentclearing.com/cgi-bin/mas/split.cgi";
$mername = "My Company Name";
$merchant_live_mode = "yes";
$sc_niftypay_submit = 'agora.cgi?secpicserve=submit_order.gif';
$sc_niftypay_change = 'agora.cgi?secpicserve=make_changes.gif';
$sc_niftypay_verify_message = "Please verify the information below.  When you are confident it is correct, click on the \'Submit Order For Processing\' button to go to the secure NiftyPay site where you will enter the remainder of your payment information.";
$sc_niftypay_order_desc = "NiftyPay - AgoraCart Online Order";
$sc_display_checkout_tos = "no";
$sc_tos_display_address = qq'AgoraCart<br>
123 Main<br>
Salt Lick City, UT 88888<br>
123-456-7890';
$sc_niftypay_top_message = "top message";
$NiftyPay_order_ok_final_msg_tbl = "";
$NiftyPay_merch_message = "<br> Thank you for using AgoraCart";
$NiftyPay_enablebodytags = "1";
$NiftyPay_bodyback = "white";
$NiftyPay_fontcolor = "black";
$NiftyPay_linkcolor = "\#0000FF";
$NiftyPay_backimage_URL = "";
$NiftyPay_Logo_URL = "";
$acceptcards = "1";
$acceptvisa = "1";
$acceptmastercard = "1";
$acceptdiscover = "1";
$acceptamex = "0";
$acceptdiners = "0";
$acceptchecks = "0";
$accepteft = "0";
$altaddr = "0";
$email_text = "<!--agorascript-pre
  return \$NiftyPay_prod_in_cart;
-->";
1;
