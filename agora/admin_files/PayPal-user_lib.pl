$sc_gateway_username = 'user@domain.com';
$sc_button_image_URL = "https://images.paypal.com/images/x-click-but6.gif";
$sc_paypal_change = "agora.cgi?secpicserve=make_changes.gif";
$sc_order_script_url = "https://www.paypal.com/xclick/business";
$sc_paypal_order_name = 'PayPal AgoraCart Purchase, INV# ';
$sc_paypal_order_number = '$sc_verify_inv_no';
$sc_currency_id = "USD";
$sc_paypal_address_override = '1';
$sc_paypal_top_message = qq'';
$sc_paypal_shipping_message = qq'';
$sc_paypal_special_message = qq'';
$sc_use_custom_paypal_url_logic = qq'no';
$sc_custom_paypal_url_logic = qq``;
$sc_paypal_verify_message = qq'Please verify the information below.  When you are confident it is correct, click on the \'Submit Order For Processing\' button to go to the Paypal site where you will enter your payment information.';
$sc_display_checkout_tos = qq'no';
$sc_tos_display_address = qq'AgoraCart<br>
123 Main<br>
Salt Lick City, UT 88888<br>
123-456-7890';
$order_ok_final_msg_tbl = qq'';
#
1;# We are a Library
