$sc_gateway_username = "AgoraPay ID here";
$sc_order_script_url = "https://secure.paymentclearing.com/cgi-bin/mas/split.cgi";
$mername = "My Company Name";
$merchant_live_mode = "yes";
$sc_agorapay_submit = 'agora.cgi?secpicserve=submit_order.gif';
$sc_agorapay_change = 'agora.cgi?secpicserve=make_changes.gif';
$sc_agorapay_verify_message = "Please verify the information below.  When you are confident it is correct, click on the \'Submit Order For Processing\' button to go to the secure AgoraPay site where you will enter the remainder of your payment information.";
$sc_agorapay_order_desc = "AgoraPay - AgoraCart Online Order";
$sc_display_checkout_tos = "no";
$sc_tos_display_address = qq'AgoraCart<br>
123 Main<br>
Salt Lick City, UT 88888<br>
123-456-7890';
$sc_agorapay_top_message = "top message";
$AgoraPay_order_ok_final_msg_tbl = "";
$AgoraPay_merch_message = "<br> Thank you for using AgoraCart";
$AgoraPay_enablebodytags = "1";
$AgoraPay_bodyback = "white";
$AgoraPay_fontcolor = "black";
$AgoraPay_linkcolor = "\#0000FF";
$AgoraPay_backimage_URL = "";
$AgoraPay_Logo_URL = "";
$acceptcards = "1";
$acceptvisa = "1";
$acceptmastercard = "1";
$acceptdiscover = "1";
$acceptamex = "0";
$acceptdiners = "0";
$acceptchecks = "0";
$accepteft = "0";
$altaddr = "0";
$email_text = "<!--agorascript-pre
  return \$AgoraPay_prod_in_cart;
-->";
1;
