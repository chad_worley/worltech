$sc_gateway_username = "iTransact ID here";
$sc_order_script_url = "https://secure.paymentclearing.com/cgi-bin/mas/split.cgi";
$mername = "My Company Name";
$merchant_live_mode = "yes";
$sc_itransact_submit = 'agora.cgi?secpicserve=submit_order.gif';
$sc_itransact_change = 'agora.cgi?secpicserve=make_changes.gif';
$sc_itransact_verify_message = "Please verify the information below.  When you are confident it is correct, click on the \'Submit Order For Processing\' button to go to the secure iTransact site where you will enter the remainder of your payment information.";
$sc_itransact_order_desc = "iTransact - AgoraCart Online Order";
$sc_display_checkout_tos = "no";
$sc_tos_display_address = qq'AgoraCart<br>
123 Main<br>
Salt Lick City, UT 88888<br>
123-456-7890';
$sc_itransact_top_message = "top message";
$iTransact_order_ok_final_msg_tbl = "";
$iTransact_merch_message = "<br> Thank you for using AgoraCart";
$iTransact_enablebodytags = "1";
$iTransact_bodyback = "white";
$iTransact_fontcolor = "black";
$iTransact_linkcolor = "\#0000FF";
$iTransact_backimage_URL = "";
$iTransact_Logo_URL = "";
$acceptcards = "1";
$acceptvisa = "1";
$acceptmastercard = "1";
$acceptdiscover = "1";
$acceptamex = "0";
$acceptdiners = "0";
$acceptchecks = "0";
$accepteft = "0";
$altaddr = "0";
$email_text = "<!--agorascript-pre
  return \$iTransact_prod_in_cart;
-->";
1;
