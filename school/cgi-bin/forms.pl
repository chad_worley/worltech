#!/home/worltech/perl
    BEGIN {
    my $b__dir = (-d '/home/worltech/perl'?'/home/worltech/perl':( getpwuid($>) )[7].'/perl');
    unshift @INC,$b__dir.'5/lib/perl5',$b__dir.'5/lib/perl5/x86_64-linux',map { $b__dir . $_ } @INC;
}
    local ($buffer, @pairs, $pair, $name, $value, %FORM);
    $ENV{'REQUEST_METHOD'} =~ tr/a-z/A-Z/;
    if ($ENV{'REQUEST_METHOD'} eq "POST")
    {
        read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
    }else {
	$buffer = $ENV{'QUERY_STRING'};
    }
    @pairs = split(/&/, $buffer);
    foreach $pair (@pairs)
    {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%(..)/pack("C", hex($1))/eg;
	$FORM{$name} = $value;
    }
    $first_name = $FORM{first_name};
    $last_name  = $FORM{last_name};
print "Content-type:text/html\r\n\r\n";
print <<ENDHTML
<html>
<head>
<title>Hello - Test CGI Program</title>
</head>
<body>
<table width="200" border="1" align="center">
<tr>
<th scope="col">
<h1 align="center">Example Page For Class</h1>
</th>
</tr>
<tr>
<td scope="col">
<h2 align="center" id="header_main" font color=#FFF">CGI scripting!"</h2>
</td>
</tr>
<tr>
<td>
<h2>Hello $first_name $last_name - This is how you use CGI to parse session data.</h2>
</td>
</tr>
</table>
</body>
</html>
ENDHTML
