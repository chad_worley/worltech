#!/home/worltech/perl
    use CGI; 
    use warnings;
    local ($buffer, @pairs, $pair, $name, $value, %FORM);
    $ENV{'REQUEST_METHOD'} =~ tr/a-z/A-Z/;
    if ($ENV{'REQUEST_METHOD'} eq "POST")
    {
        read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
    }else {
	$buffer = $ENV{'QUERY_STRING'};
    }
    @pairs = split(/&/, $buffer);
    foreach $pair (@pairs)
    {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%(..)/pack("C", hex($1))/eg;
	$FORM{$name} = $value;
    }
    $first_name = $FORM{first_name};
    $last_name  = $FORM{last_name};
print "Content-type:text/html\r\n\r\n";
print <<ENDHTML
<html>
<head>
<title>Hello - Test CGI Program</title>
</head>
<body>
<h2 align="center"> CGI scripting! </h2>
<h2>Hello $first_name $last_name - This is how you use CGI to parse session data.</h2>
</body>
</html>
ENDHTML
